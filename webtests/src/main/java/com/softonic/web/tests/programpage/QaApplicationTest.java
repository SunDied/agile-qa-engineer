package com.softonic.web.tests.programpage;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

/**
 * Program Page Test on Selenium WebDriver.
 *
 * @author thijs.vandewynckel
 */
public class QaApplicationTest{
	static WebDriver driver;
	//Before Test driver initialization
	@BeforeTest
	public void driverSetup(){
		driver = new ChromeDriver();
		driver.get("https://facebook.en.softonic.com/web-apps");
		driver.manage().window().maximize();
		
	}
	
	

  private static final String TEST_DESCRIPTION = "Register a new user to Softonic from the softonic facebook page";

  @Test(description = TEST_DESCRIPTION)
  public void verifyQaApplication(){
	  
	  //Go to Login/register form
	  
	  WebElement loginButton = driver.findElement(By.className("icon-avatar"));
	  
	  loginButton.click();
	  WebDriverWait wait = new WebDriverWait(driver, 10);
	  wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("gigya-screen-dialog-main")));
	  
	  
	  WebElement signUpNowLink = driver.findElement(By.xpath(".//a[@data-switch-screen='gigya-register-screen']"));
	  signUpNowLink.click();
	  
	  //wait.until(ExpectedConditions.presenceOfElementLocated(By.className("gigya-input-submit")));
	  
	  String dataEmail = "drc90s@gmail.com";
	  String dataFirstName = "David";
	  String dataLastName = "Romero";
	  String dataPassword = "UnaContrasenaSegura";
	  
	  wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//div[@id='register-site-login']/div/div[@class='gigya-layout-row']/div/input")));
	  
	  WebElement registerEmail = driver.findElement(By.xpath(".//div[@id='register-site-login']/div/div[@class='gigya-layout-row']/div/input"));
	  WebElement registerFirstName = driver.findElement(By.name("profile.firstName"));
	  WebElement registerLastName = driver.findElement(By.name("profile.lastName"));
	  WebElement registerPassword = driver.findElement(By.xpath(".//*[@id='password-row']/div[1]/div/input"));
	  WebElement registerretypePassword = driver.findElement(By.xpath(".//*[@id='password-row']/div[2]/div/input"));
	  WebElement registerSubmit = driver.findElement(By.className("gigya-input-submit"));
	  
	  assertTrue(registerSubmit.isDisplayed());
	  assertTrue(registerFirstName.isDisplayed());
	  assertTrue(registerLastName.isDisplayed());
	  assertTrue(registerPassword.isDisplayed());
	  assertTrue(registerretypePassword.isDisplayed());
	  assertTrue(registerSubmit.isDisplayed());

	  //Check required inputs
	  
	  registerSubmit.click();
	  	  
	  assertEquals(driver.findElement(By.xpath(".//div[@id='register-site-login']/div/div[@class='gigya-layout-row']/div/span")).getText(), "This field is required");
	  assertEquals(driver.findElement(By.xpath(".//div[@id='names-row']/div/div/span[@data-bound-to='profile.firstName']")).getText(), "This field is required");
	  assertEquals(driver.findElement(By.xpath(".//div[@id='names-row']/div/div/span[@data-bound-to='profile.lastName']")).getText(), "This field is required");
	  assertEquals(driver.findElement(By.xpath(".//*[@id='password-row']/div[1]/div/span[@data-bound-to='password']")).getText(), "This field is required");
	  assertEquals(driver.findElement(By.xpath(".//*[@id='password-row']/div[2]/div/span[@data-bound-to='passwordRetype']")).getText(), "This field is required");
	  
	  //Check email validation functionality
	  
	  registerEmail.sendKeys("asd");
	
	  wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("input.gigya-pending")));
	  assertTrue(driver.findElement(By.xpath(".//div[@id='register-site-login']/div/div[@class='gigya-layout-row']/div/input[contains(@class,'gigya-error')]")).isDisplayed());  	  
	  assertEquals(driver.findElement(By.xpath(".//div[@id='register-site-login']/div/div[@class='gigya-layout-row']/div/span")).getText(), "E-mail address is invalid.");

	  registerEmail.sendKeys("@asd.com");
	  
	  wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("input.gigya-pending")));
	  assertTrue(driver.findElement(By.xpath(".//div[@id='register-site-login']/div/div[@class='gigya-layout-row']/div/input[contains(@class,'gigya-error')]")).isDisplayed());  
	  assertEquals(driver.findElement(By.xpath(".//div[@id='register-site-login']/div/div[@class='gigya-layout-row']/div/span")).getText(), "An account with this email address already exists");
	  
	  
	  registerEmail.clear();
	  registerEmail.sendKeys(dataEmail);
	  wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("input.gigya-pending")));
	  assertTrue(wait.until(ExpectedConditions.invisibilityOfElementWithText(By.xpath(".//div[@id='register-site-login']/div/div[@class='gigya-layout-row']/div/span"), "An account with this email address already exists"))); 
	  assertTrue(driver.findElement(By.xpath(".//div[@id='register-site-login']/div/div[@class='gigya-layout-row']/div/input[contains(@class,'gigya-valid')]")).isDisplayed());  

	  
	  
	  
	  registerFirstName.sendKeys(dataFirstName);
	  assertTrue(wait.until(ExpectedConditions.invisibilityOfElementWithText(By.xpath(".//div[@id='names-row']/div/div/span[@data-bound-to='profile.firstName']"), "This field is required")));	  
	  assertTrue(driver.findElement(By.xpath(".//*[@id='names-row']/div[1]/div/input[contains(@class, 'gigya-valid')]")).isDisplayed());
	  
	  
	  
	  
	  registerLastName.sendKeys(dataLastName);
	  assertTrue(wait.until(ExpectedConditions.invisibilityOfElementWithText(By.xpath(".//div[@id='names-row']/div/div/span[@data-bound-to='profile.lastName']"), "This field is required")));	  
	  assertTrue(driver.findElement(By.xpath(".//*[@id='names-row']/div[2]/div/input[contains(@class, 'gigya-valid')]")).isDisplayed());
	  
	  
	  //Check the strength password functionality
	  
	  
	  registerPassword.sendKeys("a");
	  assertEquals(driver.findElement(By.xpath(".//div[@class='gigya-passwordStrength-text']/span[2]")).getText(), "Too weak");
	  assertTrue(driver.findElement(By.xpath(".//*[@id='password-row']/div[1]/div/input[contains(@class, 'gigya-error')]")).isDisplayed());
	  assertTrue(wait.until(ExpectedConditions.textToBePresentInElementLocated(By.xpath(".//*[@id='password-row']/div[1]/div/span[@data-bound-to='password']"), "Password does not meet complexity requirements")));
	  
	  
	  registerPassword.sendKeys("sdasd");
	  assertTrue(wait.until(ExpectedConditions.invisibilityOfElementWithText(By.xpath(".//*[@id='password-row']/div[1]/div/span[@data-bound-to='password']"), "Password does not meet complexity requirements")));	  
	  assertEquals(driver.findElement(By.xpath(".//div[@class='gigya-passwordStrength-text']/span[2]")).getText(), "Fair");
	  assertTrue(driver.findElement(By.xpath(".//*[@id='password-row']/div[1]/div/input[contains(@class, 'gigya-valid')]")).isDisplayed());

	  
	  registerPassword.sendKeys("A");
	  assertEquals(driver.findElement(By.xpath(".//div[@class='gigya-passwordStrength-text']/span[2]")).getText(), "Strong");
	  assertTrue(driver.findElement(By.xpath(".//*[@id='password-row']/div[1]/div/input[contains(@class, 'gigya-valid')]")).isDisplayed());
	  
	  registerPassword.clear();
	  registerPassword.sendKeys("asdasd'");
	  assertEquals(driver.findElement(By.xpath(".//div[@class='gigya-passwordStrength-text']/span[2]")).getText(), "Strong");
	  wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='password-row']/div[1]/div/input[contains(@class, 'gigya-valid')]")));
	  
	  registerPassword.clear();
	  registerPassword.sendKeys("asdasd1");
	  assertEquals(driver.findElement(By.xpath(".//div[@class='gigya-passwordStrength-text']/span[2]")).getText(), "Strong");
	  wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='password-row']/div[1]/div/input[contains(@class, 'gigya-valid')]")));
	  
	  
	  registerretypePassword.sendKeys("a");
	  assertTrue(wait.until(ExpectedConditions.invisibilityOfElementWithText(By.xpath(".//*[@id='password-row']/div[2]/div/span[@data-bound-to='passwordRetype']"), "This field is required")));	  
	  assertEquals(driver.findElement(By.xpath(".//*[@id='password-row']/div[2]/div/span[@data-bound-to='passwordRetype']")).getText(), "Passwords do not match");

	  registerPassword.clear();
	  registerretypePassword.clear();
	  
	  registerPassword.sendKeys(dataPassword);
	  assertEquals(driver.findElement(By.xpath(".//div[@class='gigya-passwordStrength-text']/span[2]")).getText(), "Strong");
	  wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='password-row']/div[1]/div/input[contains(@class, 'gigya-valid')]")));

	  
	  registerretypePassword.sendKeys(dataPassword);	  
	  assertTrue(wait.until(ExpectedConditions.invisibilityOfElementWithText(By.xpath(".//*[@id='password-row']/div[2]/div/span[@data-bound-to='passwordRetype']"), "This field is required")));	  
	  assertTrue(driver.findElement(By.xpath(".//*[@id='password-row']/div[1]/div/input[contains(@class, 'gigya-valid')]")).isDisplayed());
    }
  
  
  
  @AfterTest
  public void closeDriver(){
	  driver.close();
  }
}
