package com.softonic.web.tests;

import com.softonic.framework.selenium.RemoteWebDriverSession;
import com.softonic.framework.selenium.tests.BaseWebDriverTest;

/**
 * Base class for all web suite tests.
 *
 * @author thijs.vandewynckel
 */
public class BaseWebTest extends BaseWebDriverTest {

  @Override
  protected void openStartUrl() {
    super.openStartUrl();
    RemoteWebDriverSession.acceptAlertIfPresent(driver());
    driver().get(testEnvironment().getStartUrl());
  }
}
