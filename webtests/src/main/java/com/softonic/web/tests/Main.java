package com.softonic.web.tests;

import com.softonic.framework.Runner;

/**
 * Java entry class. Runs TestNG programatically.
 *
 * @author thijs.vandewynckel
 */
public final class Main {

  private Main() {
  }

  public static void main(String[] args) {
    Runner.execute();
  }
}
