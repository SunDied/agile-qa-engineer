package com.softonic.web.pages.programpage;

import java.util.List;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.softonic.web.pages.AbstractSoftonicWebPage;

/**
 * Contains methods to interact with the Softonic Program Page Method from old
 * and new program page.
 *
 * @author thijs.vandewynckel
 */
public class ProgramPage extends AbstractSoftonicWebPage {

  @FindBy(xpath = "")
  private List<WebElement> exampleElement1;

  public ProgramPage(WebDriver driver) {
    super(driver);
  }
}
