package com.softonic.web.pages.downloads;

import org.openqa.selenium.WebDriver;

import com.softonic.web.pages.AbstractSoftonicWebPage;

/**
 * Contains methods to interact with the Softonic normal postdownload page.
 *
 * @author thijs.vandewynckel
 */
public class PostDownloadPage extends AbstractSoftonicWebPage {

  public PostDownloadPage(WebDriver driver) {
    super(driver);
  }
}
