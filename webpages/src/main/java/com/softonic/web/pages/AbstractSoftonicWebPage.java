package com.softonic.web.pages;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.softonic.framework.logger.TestLogger;
import com.softonic.framework.selenium.pages.AbstractWebPage;

/**
 * Contains methods to interact with elements common to all Softonic Web pages.
 *
 * @author thijs.vandewynckel
 */
public class AbstractSoftonicWebPage extends AbstractWebPage {

  protected static final long PAGE_LOAD_TIMEOUT = 10000; // ms

  private static final TestLogger LOGGER = TestLogger.getInstance();

  public AbstractSoftonicWebPage(WebDriver driver) {
    super(driver);
    // Checks that there are no more display ads to load
    waitForPageElementsLoaded(PAGE_LOAD_TIMEOUT);
  }

  // Checks page every few ms to see if all display ads loaded, if not page should be loaded
  private void waitForPageElementsLoaded(long timeoutMs) {
    LOGGER.logComment("Waiting for page to load.");
    final long timeSlice = 1000; // check every second
    int previous;
    int current = 0;
    do {
      previous = current;
      timeoutMs -= timeSlice;
      current = driver.findElements(By.id("content")).size();
    } while (current != previous && timeoutMs > 0);
    if (timeoutMs <= 500) {
      LOGGER.logComment("Timeout exceeded waiting for page to load, proceeding anyway. "
          + "Test might fail due to page not being completely loaded.");
    }
  }
}
