package com.softonic.web.pages.downloads;

import org.openqa.selenium.WebDriver;

import com.softonic.web.pages.AbstractSoftonicWebPage;

/**
 * Contains methods to interact with Softonic Download Program Page.
 * <p>
 * eg. http://google-chrome.en.softonic.com/mac/download
 *
 * @author thijs.vandewynckel
 */
public class DownloadPage extends AbstractSoftonicWebPage {

  public DownloadPage(WebDriver driver) {
    super(driver);
  }
}
